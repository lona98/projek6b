<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('header');?>

<h2 align="center">MANAJEMEN DATA  MAHASISWA</h2>
<hr>
    <b>From Tambah Data Mahasiswa</b>

<hr>
<form action="<?php echo site_url('mahasiswa/update');?>" method="post">
<table width="100%" border="0">
     <tr>
     <td width="150">NIM</td>
     <td width="5">:</td>
     <td>
            <input type="text" value="<?php echo $mhs->nim;?>" disabled>
            <input type="hidden" name="nim" required value="<?php echo $mhs->nim;?>">
     </td>
</tr>     
<tr>
     <td width="150">Nama Mahasiswa</td>
     <td width="5">:</td>
     <td>
     	  <input type="text" name="nama" required value="<?php echo $mhs->nama;?>">
     </td>
</tr>	
<tr>
     <td width="150">Kelas</td>
     <td width="5">:</td>
     <td>
            <select name="kelas">
                <?php
                $kls[1] = NULL;
                $kls[2] = NULL;
              
                if($mhs->kelas == "12.6A.30") { $kls[1] = "selected";}
                if($mhs->kelas == "12.6B.30") { $kls[2] = "selected";}
               

                ?>
               <option value="12.6A.30" <?php echo $kls[1];?> >12.6A.30</option>
               <option value="12.6B.30" <?php echo $kls[2];?> >12.6B.30</option>
          </select>     
     </td>
</tr>     
<tr>
     <td width="150"></td>
     <td width="5">:</td>
     <td>
     	  <input type="submit" name="btn" value="simpan Data">
     </td>
</tr>	
</table>
</form>

<?php $this->load->view('footer');?>