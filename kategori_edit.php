<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('header');?>

<h2 align="center">MANAJEMEN DATA KATEGORI</h2>
<hr>
	<b>Form Edit Data Kategori</b>
<hr>
<form action="<?php echo site_url('kategori/update');?>" method="post">
<input type="hidden" name="idk" value="<?php echo $ktg->idk;?>">
<table width="100%" border="0">
<tr>
	<td width="150">Nama Kategori</td>
	<td width="5">:</td>
	<td>
		<input type="text" name="nmkategori" required value="<?php echo $ktg->nmkategori;?>">
	</td>
</tr>
<tr>
	<td width="150"></td>
	<td width="5"></td>
	<td>
		<input type="submit" name="btn" value="Simpan Data">
	</td>
</tr>
</table>
</form>
<?php $this->load->view('footer');?>
